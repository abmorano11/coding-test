const express = require('express');
const cors = require('cors');
const app = express();
app.use(express.json());
app.use(cors())
let data =[
    {   
        index: 0,
        firstName: 'firstName',
        lastName: 'lastName',
        deliveryAddress: 'deliveryAddress',
        billingAddress: 'billingAddress'
    },
    {
        index: 1,
        firstName: 'firstName 1',
        lastName: 'lastName 1',
        deliveryAddress: 'deliveryAddress 1',
        billingAddress: 'billingAddress 1'
    },
    {
        index: 2,
        firstName: 'firstName 2',
        lastName: 'lastName 2',
        deliveryAddress: 'deliveryAddress 2',
        billingAddress: 'billingAddress 2'
    },
    {
        index: 3,
        firstName: 'firstName 3',
        lastName: 'lastName 3',
        deliveryAddress: 'deliveryAddress 3',
        billingAddress: 'billingAddress 3'
    }
]
app.listen(3000, function() {
    console.log('listening on 3000')
})
app.get('/items', function (req, res) {
    res.send(data)
})
app.post('/items', function (req, res) {
    const item = req.body
    data.push({
        firstName: item.firstName,
        lastName: item.lastName,
        deliveryAddress: item.deliveryAddress,
        billingAddress: item.billingAddress
    })
    res.send(data)
})
app.put('/items', function (req, res) {
    const item = req.body
    if(!isNaN(item.index) && item.index < data.length)
    data[item.index] = {
        firstName: item.firstName,
        lastName: item.lastName,
        deliveryAddress: item.deliveryAddress,
        billingAddress: item.billingAddress
    }
    res.send(data)
})
app.delete('/items', function (req, res) {
    const form = req.body
    if(!isNaN(form.index) && form.index < data.length)
    data.splice(form.index, 1)
    data = data.map((item, ind) => {
        item.index = ind
        return item
    })
    res.send(data)
})